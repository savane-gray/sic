.de capitalize
.	tr aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ
.	ds letter \\$*
.	substring letter 0 0
.	di uppercase
.	nop \\*[letter]
.	br
.	di
.	asciify uppercase
.	chop uppercase
.	tr aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz
.	ds letter \\*[uppercase]
.	ds temp \\$*
.	substring temp 1
.	as letter \\*[temp]
.	nop \\*[letter]
..
.if "\*[.T]"html" \{\
.SH DOWNLOADS
.PP
\fB
.capitalize \*[package]
\fR
is available for download from
.URL \V[DOWNLOAD_RELEASES_URL]/\*[package] "this location" .
.PP
The latest version is
.URL \V[DOWNLOAD_RELEASES_URL]/\*[package]/\*[package]-\*[version].tar.gz \*[package]-\*[version] .
.PP
Recent news, changes and bugfixes can be tracked from the
.URL \V[VCS_SITE_URL]/projects/\*[package] "project's development page" .

\}

