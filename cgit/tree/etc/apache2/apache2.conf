ServerRoot "/usr"
Listen 80

LoadModule authz_host_module /usr/lib/apache2/modules/mod_authz_host.so
LoadModule authz_core_module /usr/lib/apache2/modules/mod_authz_core.so
LoadModule reqtimeout_module /usr/lib/apache2/modules/mod_reqtimeout.so
LoadModule include_module /usr/lib/apache2/modules/mod_include.so
LoadModule filter_module /usr/lib/apache2/modules/mod_filter.so
LoadModule deflate_module /usr/lib/apache2/modules/mod_deflate.so
LoadModule mime_module /usr/lib/apache2/modules/mod_mime.so
LoadModule env_module /usr/lib/apache2/modules/mod_env.so
LoadModule headers_module /usr/lib/apache2/modules/mod_headers.so
LoadModule setenvif_module /usr/lib/apache2/modules/mod_setenvif.so
LoadModule remoteip_module /usr/lib/apache2/modules/mod_remoteip.so
LoadModule mpm_event_module /usr/lib/apache2/modules/mod_mpm_event.so
LoadModule cgi_module /usr/lib/apache2/modules/mod_cgi.so
LoadModule negotiation_module /usr/lib/apache2/modules/mod_negotiation.so
LoadModule dir_module /usr/lib/apache2/modules/mod_dir.so
LoadModule alias_module /usr/lib/apache2/modules/mod_alias.so
LoadModule rewrite_module /usr/lib/apache2/modules/mod_rewrite.so

User www-data
Group www-data

ServerAdmin ${SV_ADMIN_EMAIL}
ServerName cgit:80
ServerTokens Prod

<Directory />
    AllowOverride none
    Require all denied
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "| /usr/bin/logger -thttpd -plocal0.err"
LogLevel warn

LogFormat "%a %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
CustomLog "| /usr/bin/logger -thttpd -plocal0.notice" combined

ScriptAlias /cgit/ "/usr/lib/cgit/cgit.cgi/"
RedirectMatch ^/cgit$ /cgit/
Alias /cgit-css "/usr/share/cgit/"
<Directory "/usr/lib/cgit/">
   AllowOverride None
   Options Includes ExecCGI FollowSymlinks
   AddHandler cgi-script .cgi
   Require all granted
   AddOutputFilterByType INCLUDES;DEFLATE text/html
</Directory>
<Directory "/usr/share/cgit/">
    Options All +Indexes
    AllowOverride None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/apache2/mime.types
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz
</IfModule>

Include /etc/apache2/extra/httpd-multilang-errordoc.conf
Include /etc/apache2/extra/httpd-languages.conf

#
# MPM event configuration
#
PidFile "/run/apache2.pid"

StartServers             3
MinSpareThreads         75
MaxSpareThreads        250
ThreadsPerChild         25
MaxRequestWorkers      400
MaxConnectionsPerChild   0

#
# mod_remoteip
#
RemoteIPHeader X-Forwarded-For
RemoteIPTrustedProxy ${SV_TRUSTED_PROXY}

Define GIT_PROJECT_ROOT /srv/repo/gitroot

Alias   /repo/    "${GIT_PROJECT_ROOT}/"
Alias   /webgit/  "/srv/repo/webgit/"

<Directory "${GIT_PROJECT_ROOT}/">
    Options All +Indexes
    AllowOverride None
    Require all granted
</Directory>
<Directory "/srv/repo/webgit">
    Options All +Indexes
    AllowOverride None
    Require all granted
</Directory>
<Directory /usr/lib/git-core>
    Options -Indexes -FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

DocumentRoot    "/var/www/html"
<Directory "/var/www/html">
    Options None
    Require all granted
</Directory>

SetEnvIf Host "^web\.git\." CGIT_CONFIG=/etc/web.cgitrc
PassEnv SV_ADMIN_EMAIL

# The following is based on:
# https://git.seveas.net/apache-gitweb-cgit-smart-http.html

RewriteEngine on
RewriteRule ^/robots.txt        -       [L]
RewriteRule ^/(.*\.git(|(/(?!(HEAD|info|objects|git-(upload|receive)-pack)).*)))?$ \
                    /usr/lib/cgit/cgit.cgi/$1

SetEnv GIT_HTTP_EXPORT_ALL
SetEnv GIT_PROJECT_ROOT ${GIT_PROJECT_ROOT}
ScriptAliasMatch "^/(.*\.git/(HEAD|info/refs))$" \
                         /usr/lib/git-core/git-http-backend/$1
ScriptAliasMatch "^/(.*\.git/git-(upload|receive)-pack)$" \
                         /usr/lib/git-core/git-http-backend/$1

AliasMatch "^/(.*\.git/objects/(info/[^/]+|[0-9a-f]{2}/[0-9a-f]{38}|pack/pack-[0-9a-f]{40}\.(pack|idx)))" \
                 ${GIT_PROJECT_ROOT}/$1

