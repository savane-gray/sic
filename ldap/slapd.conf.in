#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openssh-lpk.schema
include		/etc/ldap/schema/pgp-keyserver.schema
include		/etc/ldap/schema/misc.schema

pidfile		/run/slapd/slapd.pid
argsfile	/run/slapd/slapd.args

# Load dynamic backend modules:
modulepath	/usr/lib/ldap
moduleload	back_mdb

access to dn.base="" by * read
access to dn.base="cn=Subschema" by * read
access to *
	by peername.ip=127.0.0.1 read
	by * break
access to *
	by dn="$LDAP_WRITER" write
	by peername.ip=$LDAP_READER_NET read
	by self write
	by users read
	by anonymous auth

#######################################################################
# MDB database definitions
#######################################################################

database	mdb
suffix		"$LDAP_BASE"
rootdn		"$LDAP_ROOTDN"
# Cleartext passwords, especially for the rootdn, should
# be avoid.  See slappasswd(8) and slapd.conf(5) for details.
# Use of strong authentication encouraged.
rootpw		$LDAP_ROOTPW
# The database directory MUST exist prior to running slapd AND 
# should only be accessible by the slapd and slap tools.
# Mode 700 recommended.
directory	/var/lib/ldap
# Indices to maintain
index	objectClass	eq
index	ou		eq
index	cn		eq
index   uid             eq
index   uidNumber       eq
index   memberUid       eq
index   gidNumber       eq
index	pgpKeyID	eq
index	pgpCertID	eq
index	uniqueMember	eq