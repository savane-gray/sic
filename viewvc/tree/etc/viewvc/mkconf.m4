divert(-1)
define(`NB',`dnl')
dnl REPOSITORY(type,dir)
define(`REPOSITORY',`ifdef(`MAKE_KV',`divert(0)dnl
[repo]
root = ifelse(`$3',,`patsubst(`$2',`.*/')',$3)
type = `$1'
divert(-1)
',`
 define(`REPO_TYPE',`$1')
 define(`REPO_PARENT',`$2')
 define(`KEYWORDS',patsubst(__file__,`\.cfin$').kv)')')

define(`BEGIN',`dnl
divert(0)dnl
# -*- buffer-read-only: t -*- vi: set ro:
# THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
')


