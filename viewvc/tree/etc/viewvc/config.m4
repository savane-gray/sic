BEGIN
NB ---------------------------------------------------------------------------
NB 
NB  Configuration file for ViewVC
NB 
NB  Information on ViewVC is located at the following web site:
NB      http://viewvc.org/
NB 
NB ---------------------------------------------------------------------------

NB 
NB  BASIC CONFIGURATION
NB 
NB  For correct operation, you will probably need to change the following
NB  configuration variables:
NB 
NB     cvs_roots (for CVS)
NB     svn_roots (for Subversion)
NB     root_parents (for CVS or Subversion)
NB     default_root
NB     rcs_path or cvsnt_exe_path
NB     mime_types_file 
NB 
NB  It is usually desirable to change the following variables:
NB 
NB     address
NB     forbidden
NB 
NB     use_enscript
NB     use_cvsgraph
NB 
NB  For Python source colorization:
NB 
NB     py2html_path
NB 
NB  To optimize delivery of ViewVC static files:
NB 
NB     docroot
NB 
NB  To customize the display of ViewVC for your site:
NB 
NB     template_dir
NB     the [templates] override section
NB 

NB 
NB  FORMAT INFORMATION
NB 
NB  This file is delineated by sections, specified in [brackets]. Within each
NB  section, are a number of configuration settings. These settings take the
NB  form of: name = value. Values may be continued on the following line by
NB  indenting the continued line.
NB 
NB  WARNING: indentation *always* means continuation. name=value lines should
NB           always start in column zero.
NB 
NB  Comments should always start in column zero, and are identified with "NB ".
NB 
NB  Certain configuration settings may have multiple values. These should be
NB  separated by a comma. The settings where this is allowed are noted below.
NB 
NB  Any other setting that requires special syntax is noted at that setting.
NB  

NB ---------------------------------------------------------------------------
[general]

NB 
NB  This setting specifies each of the CVS roots on your system and assigns
NB  names to them. Each root should be given by a "name: path" value. Multiple
NB  roots should be separated by commas and can be placed on separate lines.
NB 
NB cvs_roots = Development : /home/puszcza/cvsroot,
NB 	Mirroring : /home/gnu/cvsroot,
NB 	Web : /home/puszcza/webcvs
NB 
NB  This setting specifies each of the Subversion roots (repositories)
NB  on your system and assigns names to them. Each root should be given
NB  by a "name: path" value. Multiple roots should be separated by
NB  commas and can be placed on separate lines.
NB 
NB svn_roots = svn: /home/svnrepos

NB  The 'root_parents' setting specifies a list of directories in which
NB  any number of repositories may reside.  Rather than force you to add
NB  a new entry to 'cvs_roots' or 'svn_roots' each time you create a new
NB  repository, ViewVC rewards you for organising all your repositories
NB  under a few parent directories by allowing you to simply specifiy
NB  just those parent directories.  ViewVC will then notice each
NB  repository in that directory as a new root whose name is the
NB  subdirectory of the parent path in which that repository lives.
NB 
NB  You can specify multiple parent paths separated by commas or new lines.
NB 
NB  Caution: these names can, of course, clash with names you have
NB  defined in your cvs_roots or svn_roots configuration items.  If this
NB  occurs, you can either rename the offending repository on disk, or
NB  grant new names to the clashing item in cvs_roots or svn_roots.
NB  Each parent path is processed sequentially, so repositories under
NB  later parent paths may override earlier ones.
NB 
root_parents = REPO_PARENT : REPO_TYPE


NB  this is the name of the default root
NB  (ignored when root_as_url_component is turned on)
NB default_root = default

NB  uncomment if the RCS binaries are not on the standard path
NB rcs_path = /usr/bin/

NB  ViewVC can use CVSNT (www.cvsnt.org) instead of the RCS 
NB  utilities to retrieve information from CVS repositories.
NB  To enable use of CVSNT, set the "cvsnt_exe_path" value
NB  to the path of the CVSNT executable. (If CVSNT is on the
NB  standard path, you can also set it to the name of the
NB  CVSNT executable). By default "cvsnt_exe_path" is set to
NB  "cvs" on Windows and is not set on other platforms.
NB 
NB cvsnt_exe_path =
NB cvsnt_exe_path = cvs
NB cvsnt_exe_path = K:\Program Files\cvsnt\cvs.exe
NB cvsnt_exe_path = /usr/bin/cvs

NB  use rcsparse python module to retrieve cvs repository information instead
NB  of invoking rcs utilities. this feature is experimental
use_rcsparse = 0

NB  uncomment if the svn command-line utilities are not on the standard path
NB svn_path = /usr/bin/

NB 
NB  This is a pathname to a MIME types file to help viewvc to guess the
NB  correct MIME type on checkout.
NB 
NB  If you are having problems with the default guess on the MIME type, then
NB  uncomment this option and point it at a MIME type file.
NB 
NB  For example, you can use the mime.types from apache here:
NB mime_types_file = /usr/local/apache/conf/mime.types

NB  This address is shown in the footer of the generated pages. 
NB  It must be replaced with the address of the local repository maintainer.
NB address = gray@gnu.org.ua

NB 
NB  This should contain a list of modules in the repository that should not be
NB  displayed (by default or by explicit path specification).
NB 
NB  This configuration can be a simple list of modules, or it can get quite
NB  complex:
NB 
NB    *) The "!" can be used before a module to explicitly state that it
NB       is NOT forbidden. Whenever this form is seen, then all modules will
NB       be forbidden unless one of the "!" modules match.
NB 
NB    *) Shell-style "glob" expressions may be used. "*" will match any
NB       sequence of zero or more characters, "?" will match any single
NB       character, "[seq]" will match any character in seq, and "[!seq]"
NB       will match any character not in seq.
NB 
NB    *) Tests are performed in sequence. The first match will terminate the
NB       testing. This allows for more complex allow/deny patterns.
NB 
NB  Tests are case-sensitive.
NB 
forbidden = CVSROOT

NB  Some examples:
NB 
NB  Disallow "example" but allow all others:
NB    forbidden = example
NB 
NB  Disallow "example1" and "example2" but allow all others:
NB    forbidden = example1, example2
NB 
NB  Allow *only* "example1" and "example2":
NB    forbidden = !example1, !example2
NB 
NB  Forbid modules starting with "x":
NB    forbidden = x*
NB 
NB  Allow modules starting with "x" but no others:
NB    forbidden = !x*
NB 
NB  Allow "xml", forbid other modules starting with "x", and allow the rest:
NB    forbidden = !xml, x*, !*
NB 

NB 
NB  This option provides a mechanism for custom key/value pairs to be
NB  available to templates. These are stored in key/value files (KV files).
NB 
NB  Pathnames to the KV files are listed here, specified as absolute paths
NB  or relative to this configuration file. The kV files follow the same
NB  format as this configuration file. It may have multiple, user-defined
NB  sections, and user-defined options in those sections. These are all
NB  placed into a structure available to the templates as:
NB 
NB     kv.SECTION.OPTION
NB 
NB  Note that an option name can be dotted. For example:
NB 
NB     [my_images]
NB     logos.small = /images/small-logo.png
NB     logos.big = /images/big-logo.png
NB 
NB  Templates can use these with a directive like: [kv.my_images.logos.small]
NB 
NB  Note that sections across multiple files will be merged. If two files
NB  have a [my_images] section, then the options will be merged together.
NB  If two files have the same option name in a section, then one will
NB  overwrite the other (it is unspecified regarding which "wins").
NB 
NB  To further categorize the KV files, and how the values are provided to
NB  the templates, a KV file name may be annotated with an additional level
NB  of dotted naming. For example:
NB 
NB     kv_files = [asf]kv/images.conf
NB 
NB  Assuming the same section as above, the template would refer to an image
NB  using [kv.asf.my_images.logos.small]
NB 
NB  Lastly, it is possible to use %lang% in the filenames to specify a
NB  substitution of the selected language-tag.
NB 
kv_files = gnu.kv, KEYWORDS

NB  example:
NB  kv_files = kv/file1.conf, kv/file2.conf, [i18n]kv/%lang%_data.conf
NB 

NB 
NB  The languages available to ViewVC. There are several i18n mechanisms
NB  available:
NB 
NB    1) using key/value extension system and reading KV files based on
NB       the selected language
NB    2) GNU gettext to substitute text in the templates
NB    3) using different templates, based on the selected language
NB 
NB  NB NB NB  NOTE: at the moment, the GNU gettext style is not implemented
NB 
NB  This option is a comma-separated list of language-tag values. The first
NB  language-tag listed is the default language, and will be used if an
NB  Accept-Language header is not present in the request, or none of the
NB  user's requested languages are available. If there are ties on the
NB  selection of a language, then the first to appear in the list is chosen.
NB 
languages = pl, uk, pt, ca, es, fr, de, no, en-us

NB  other examples:
NB 
NB  languages = en-us, de
NB  languages = en-us, en-gb, de
NB  languages = de, fr, en-us
NB 

NB ---------------------------------------------------------------------------
[options]

NB allowed_views: List the ViewVC views which are enabled.  Views not
NB in this comma-delited list will not be served (or, will return an
NB error on attempted access).
NB See viewvc.conf for a description of each,
allowed_views = annotate, diff, markup, roots, co

NB  root_as_url_component: Interpret the first path component in the URL
NB  after the script location as the root to use.  This is an
NB  alternative to using the "root=" query key. If ViewVC is configured
NB  with multiple repositories, this results in more natural looking
NB  ViewVC URLs.
NB  Note: Enabling this option will break backwards compatibility with
NB  any old ViewCVS URL which doesn't have an explicit "root" parameter.
root_as_url_component = 1

NB  default_file_view: "log" or "co"
NB  Controls whether the default view for file URLs is a checkout view or
NB  a log view. "log" is the default for backwards compatibility with old
NB  ViewCVS URLs, but "co" has the advantage that it allows ViewVC to serve
NB  static HTML pages directly from a repository with working links
NB  to other repository files
NB  Note: Changing this option may cause old ViewCVS URLs that referred
NB  to log pages to load checkout pages instead.
default_file_view = log

NB  checkout_magic: Use checkout links with magic /*checkout*/ prefixes so
NB  checked out HTML pages can have working links to other repository files
NB  Note: This option is DEPRECATED and should not be used in new ViewVC
NB  installations. Setting "default_file_view = co" achieves the same effect
checkout_magic = 0

NB  http_expiration_time: Expiration time (in seconds) for cacheable
NB  pages served by ViewVC.  Note that in most cases, a cache aware
NB  client will only revalidate the page after it expires (using the
NB  If-Modified-Since and/or If-None-Match headers) and that browsers
NB  will also revalidate the page when the reload button is pressed.
NB  Set to 0 to disable the transmission of these caching headers.
http_expiration_time = 600

NB  generate_etags: Generate Etag headers for relevant pages to assist
NB  in browser caching.
NB    1      Generate Etags
NB    0      Don't generate Etags
generate_etags = 1

NB  sort_by: File sort order
NB    file   Sort by filename
NB    rev    Sort by revision number
NB    date   Sort by commit date
NB    author Sort by author
NB    log    Sort by log message
sort_by = file

NB  sort_group_dirs: Group directories when sorting
NB    1      Group directories together
NB    0      No grouping -- sort directories as any other item would be sorted
sort_group_dirs = 1

NB  hide_attic: Hide or show the contents of the Attic subdirectory
NB    1      Hide dead files inside Attic subdir
NB    0      Show the files which are inside the Attic subdir
hide_attic = 1

NB  log_sort: Sort order for log messages
NB    date   Sort revisions by date
NB    rev    Sort revision by revision number
NB    cvs    Don't sort them. Same order as CVS/RCS shows them.
log_sort = date

NB  diff_format: Default diff format
NB    h      Human readable
NB    u      Unified diff
NB    c      Context diff
NB    s      Side by side
NB    l      Long human readable (more context)
diff_format = h

NB  hide_cvsroot: Don't show the CVSROOT directory
NB    1      Hide CVSROOT directory
NB    0      Show CVSROOT directory
hide_cvsroot = 1

NB  set to 1 to make lines break at spaces,
NB  set to 0 to make no-break lines,
NB  set to a positive integer to make the lines cut at that length
hr_breakable = 1

NB  give out function names in human readable diffs
NB  this just makes sense if we have C-files, otherwise
NB  diff's heuristic doesn't work well ..
NB  ( '-p' option to diff)
hr_funout = 0

NB  ignore whitespaces for human readable diffs
NB  (indendation and stuff ..)
NB  ( '-w' option to diff)
hr_ignore_white = 1

NB  ignore diffs which are caused by
NB  keyword-substitution like $Id - Stuff
NB  ( '-kk' option to rcsdiff)
hr_ignore_keyword_subst = 1

NB  Enable highlighting of intraline changes in human readable diffs
NB  this feature is experimental and currently requires python 2.4
NB  
hr_intraline = 0

NB  allow annotation of files.
allow_annotate = 1

NB  allow pretty-printed version of files
allow_markup = 1

NB  allow compression with gzip of output if the Browser accepts it
NB  (HTTP_ACCEPT_ENCODING=gzip)
NB  [make sure to have gzip in the path]
allow_compress = 1

NB  The directory which contains the EZT templates used by ViewVC to
NB  customize the display of the various output views.  ViewVC looks in
NB  this directory for files with names that match the name of the view
NB  ("log", "directory", etc.) plus the ".ezt" extension.  If specified
NB  as a relative path, it is relative to the ViewVC installation
NB  directory; absolute paths may be used as well.
NB 
NB  If %lang% occurs in the pathname, then the selected language will be
NB  substituted.
NB 
template_dir = /etc/viewvc/templates

NB  Web path to a directory that contains ViewVC static files
NB  (stylesheets, images, etc.)  If set, static files will get
NB  downloaded directory from this location.  If unset, static files
NB  will be served by the ViewVC script (at a likely performance
NB  penalty, and from the "docroot" subdirectory of the directory
NB  specified by the "template_dir" option).
NB docroot = /docroot

NB  Show last changelog message for sub directories
NB  The current implementation makes many assumptions and may show the
NB  incorrect file at some times. The main assumption is that the last
NB  modified file has the newest filedate. But some CVS operations
NB  touches the file without even when a new version is not checked in,
NB  and TAG based browsing essentially puts this out of order, unless
NB  the last checkin was on the same tag as you are viewing.
NB  Enable this if you like the feature, but don't rely on correct results.
show_subdir_lastmod = 0

NB  show a portion of the most recent log entry in directory listings
show_logs = 1

NB  Show log when viewing file contents
show_log_in_markup = 1

NB  Cross filesystem copies when traversing Subversion file revision histories.
cross_copies = 1

NB  Display dates as UTC or in local time zone
use_localtime = 0
NB use_localtime = 1

NB  == Configuration defaults ==
NB  Defaults for configuration variables that shouldn't need
NB  to be configured..

NB 
NB  If you want to use Marc-Andrew Lemburg's py2html (and Just van Rossum's
NB  PyFontify) to colorize Python files, then you may need to change this
NB  variable to point to their directory location.
NB 
NB  This directory AND the standard Python path will be searched.
NB 
py2html_path = .
NB py2html_path = /usr/local/lib/python1.5/site-python

NB  the length to which the most recent log entry should be truncated when
NB  shown in the directory view
short_log_len = 80

NB  should we use 'enscript' for syntax coloring?
use_enscript = 0

NB 
NB  if the enscript program is not on the path, set this value
NB 
enscript_path =
NB  enscript_path = /usr/bin/

NB  should we use 'highlight' for syntax coloring?
NB  NOTE: use_enscript has to be 0 or enscript will be used instead
use_highlight = 0

NB 
NB  if the highlight program is not on the path, set this value
NB 
NB  highlight_path = /usr/bin

NB  should we add line numbers?
highlight_line_numbers = 1

NB  convert tabs to NB NB  spaces (use 0 for no conversion)
highlight_convert_tabs = 2

NB  use php to colorize .php and .inc files?
use_php = 0

NB  path to php executable
NB  (This should be set to the path of a PHP CLI executable, not the path
NB  to a CGI executable. If you use a CGI executable, you may see "no input file
NB  specified" or "force-cgi-redirect" errors instead of colorized source. The
NB  output of "php -v" tells you whether an given executable is CLI or CGI.)
php_exe_path = php
NB  php_exe_path = /usr/local/bin/php
NB  php_exe_path = C:\Program Files\php\cli\php.exe

NB 
NB  ViewVC can generate tarball from a repository on the fly.
NB 
allow_tar = 0
NB  allow_tar = 1

NB 
NB  Use CvsGraph. See http://www.akhphd.au.dk/~bertho/cvsgraph/ for
NB  documentation and download. 
NB 
use_cvsgraph = 0
NB  use_cvsgraph = 1

NB 
NB  if the cvsgraph program is not on the path, set this value
NB 
cvsgraph_path =
NB  cvsgraph_path = /usr/local/bin/

NB 
NB  Location of the customized cvsgraph configuration file.  
NB  You will need an absolute pathname here:
NB 
cvsgraph_conf = cvsgraph.conf

NB 
NB  Set to enable regular expression search of all files in a directory
NB 
NB  WARNING:
NB 
NB    Enabling this option can consume HUGE amounts of server time. A
NB    "checkout" must be performed on *each* file in a directory, and
NB    the result needs to be searched for a match against the regular
NB    expression.
NB 
NB 
NB  SECURITY WARNING:  Denial Of Service
NB 
NB    Since a user can enter the regular expression, it is possible for
NB    them to enter an expression with many alternatives and a lot of
NB    backtracking. Executing that search over thousands of lines over
NB    dozens of files can easily tie up a server for a long period of
NB    time.
NB 
NB  This option should only be used on sites with trusted users. It is
NB  highly inadvisable to use this on a public site.
NB 
use_re_search = 0
NB  use_re_search = 1

NB 
NB  Split directories and logs into pages.
NB  Allows ViewVC to present discrete pages to the users instead of the
NB  entire log or directory.
NB  Set use_pagesize to the number of entries you want displayed on a page.
NB 
use_pagesize = 0
NB  use_pagesize = 20

NB  Limit number of changed paths shown per commit in the Subversion revision
NB  view and in query results. This is not a hard limit (the UI provides
NB  options to show all changed paths), but it prevents ViewVC from generating
NB  enormous and hard to read pages by default when they happen to contain
NB  import or merge commits affecting hundreds or thousands of files.
NB  Set to 0 to disable the limit.
limit_changes = 100

NB ---------------------------------------------------------------------------
[templates]

NB  You can override the templates used by various ViewVC views in this
NB  section.  By default, ViewVC will look for templates in the
NB  directory specified by the "template_dir" configuration option (see
NB  the documentation for that option for details).  But if you want to
NB  use a different template for a particular view, simply uncomment the
NB  appropriate option below and specify the currect location of the EZT
NB  template file you wish to use for that view.
NB  
NB  Templates are specified relative to the installation directory, but
NB  absolute paths may also be used as well.
NB 
NB  If %lang% occurs in the pathname, then the selected language will be
NB  substituted.
NB 
NB  Note: the selected language is defined by the "languages" item in the
NB        [general] section, and based on the request's Accept-Language
NB        header.
NB 
NB directory = templates/directory.ezt
NB NB NB  an alternative directory view
NB directory = templates/dir_new.ezt   
NB log = templates/log.ezt
NB NB NB  a table-based alternative log view
NB log = templates/log_table.ezt  
NB query = templates/query.ezt
NB diff = templates/diff.ezt
NB graph = templates/graph.ezt
NB annotate = templates/annotate.ezt
NB markup = templates/markup.ezt
NB revision = templates/revision.ezt
NB query_form = templates/query_form.ezt
NB query_results = templates/query_results.ezt
NB error = templates/error.ezt
NB roots = templates/roots.ezt

NB ---------------------------------------------------------------------------
[cvsdb]

enabled = 0
NB host = localhost
NB port = 3306
NB database_name = ViewVC
NB user = 
NB passwd = 
NB readonly_user = 
NB readonly_passwd = 
NB row_limit = 1000

NB ---------------------------------------------------------------------------
[vhosts]
NB  DOC

NB  vhost1 = glob1, glob2
NB  vhost2 = glob3, glob4

NB  [vhost1-section]
NB  option = value
NB  [vhost1-othersection]
NB  option = value
NB  [vhost2-section]
NB  option = value

NB 
NB  Here is an example:
NB 
NB  [vhosts]
NB  lyra = *lyra.org
NB 
NB  [lyra-general]
NB  forbidden = hideme
NB 
NB  [lyra-options]
NB  show_logs = 0
NB 
NB  Note that "lyra" is the "canonical" name for all hosts in the lyra.org
NB  domain. This canonical name is then used within the additional, vhost-
NB  specific sections to override specific values in the common sections.
NB 

NB ---------------------------------------------------------------------------
