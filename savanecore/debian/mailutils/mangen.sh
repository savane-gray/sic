#!/bin/sh

## mangen.sh
## Copyright (C) 2004 Free Software Foundation, Inc.
##
## GNU Mailutils is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 2, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc. 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##
## All mail utilities must be already installed!
## Requires `help2man'. Assume standard `/usr/local' by default.

set -e

if test -z "$1"; then
  prefix="/usr/local"
else
  prefix=$1
fi

export LD_LIBRARY_PATH="$prefix/lib/$DEB_HOST_MULTIARCH"
export GUILE_LOAD_PATH="$(pwd)/libmu_scm:$prefix/share/guile/site"
export GUILE_AUTO_COMPILE=0
export MAILUTILS_SCM_LIBRARY_ROOT=$(pwd)

help2man="help2man"

man1="dotlock.mailutils frm.mailutils from.mailutils mailutils mail.mailutils mailutils-config messages.mailutils mimeview movemail.mailutils popauth readmsg.mailutils sieve guimb"
man8="comsatd imap4d maidag pop3d"

mail_desc="process mail messages"
mailutils_config_desc="deprecated mailutils build configuration tool"
comsatd_desc="the Comsat daemon"

mailutils_postprocess=mailutils_pp

see_also() {
    while [ $# -gt 0 ]
    do
	sub=$1
	shift
	if [ $# -gt 0 ]; then
	    delim=,
	else
	    delim=.
	fi
	echo ".BR ${sub} (1)$delim\\"
    done
}

mailutils_pp() {
    sublist=$(find $prefix/lib/mailutils -maxdepth 1 -type f -name 'mailutils-*' -printf '%f\n')
    (echo '/No commands found\./{i\'
    echo 'Available subcommands are:\'
    for sub in $sublist
    do
	cn=$(basename $sub)
	echo '.TP\'
	echo ".B mailutils ${cn##mailutils-}\\"
	$prefix/lib/mailutils/$sub --help | sed -n '2{s/$/\\/p}'
    done
    cat <<'EOF'

d}
s/Informational options/.SH OPTIONS/
/give this help list/{
s/this/short/
a\
.TP\
\\fB\\-\\-help\\fR \\fICOMMAND\\fR\
Display help for the supplied \\fICOMMAND\\fR\

}
/.SH "SEE ALSO"/a\
EOF
    see_also $sublist
    cat <<EOF

EOF

) > subhelp.sed
    sed -f subhelp.sed -i $1
    for sub in $sublist
    do
	cn=$(basename $sub)
	desc=$($prefix/lib/mailutils/$sub --help | sed -n 2p)
	echo "Creating $sub.1..."
	$help2man -N -i debian/mangen.inc -s 1 -S FSF -n "$desc" \
	    $prefix/lib/mailutils/$sub > $sub.1
    done
}

# make_sectin N DIR
make_section() {
    eval proglist=\$man$1
    for program in $proglist; do
	echo "Creating $program.$1..."
	canonical_name=$(echo ${program%%.mailutils} | tr - _)
	eval desc=\$${canonical_name}_desc
	if [ -z "$desc" ]; then
	    desc=$($prefix/$2/$program --help | \
	       sed -r -n '2s/(GNU[[:space:]]+)?[^[:space:]]+([[:space:]]*--)?[[:space:]]+//p')
        fi
	$help2man -N -i debian/mangen.inc -s $1 -S FSF -n "$desc" \
	    $prefix/$2/$program >$program.$1
	eval postprocess=\$${canonical_name}_postprocess
	if [ -n "$postprocess" ]; then
	    $postprocess $program.$1
	fi
    done
}

make_section 1 bin
make_section 8 sbin

exit 0
