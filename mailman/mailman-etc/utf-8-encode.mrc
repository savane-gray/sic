<!-- ================================================================== -->
<!--  File:
	$Id: utf-8-encode.mrc,v 1.3 2004/03/15 21:07:18 ehood Exp $
      Author:
	Earl Hood <earl @ earlhood . com>

      Description:
	MHonArc, <http://www.mhonarc.org/>, resource file to
	encode message text data into Unicode UTF-8.  This only
	works with v2.6.0, or later, of MHonArc.

      Notes:
	* This is a more general version of utf-8.mrc.	Where
	  utf-8.mrc basis its conversion via CHARSETCONVERTERS,
	  this does it via TEXTENCODE.

	  The advantage of TEXTENCODE, is that message text data,
	  including headers, are converted to UTF-8 when read.	This
	  provides a performance advantage over the CHARSETCONVERTERS
	  method, and TEXTENCODE affects all text entities in a
	  message bodies.  The CHARSETCONVERTERS method depends on
	  individual text-based MIMEFILTERS to explicitly support
	  CHARSETCONVERTERS.  TEXTENCODE is transparent to MIMEFILTERS.

  -->
<!-- ================================================================== -->

<TextEncode>
utf-8; MHonArc::UTF8::to_utf8; MHonArc/UTF8.pm
</TextEncode>

<-- With data translated to UTF-8, it simplifies CHARSETCONVERTERS -->
<CharsetConverters override>
default; mhonarc::htmlize
</CharsetConverters>

<-- Need to also register UTF-8-aware text clipping function -->
<TextClipFunc>
MHonArc::UTF8::clip; MHonArc/UTF8.pm
</TextClipFunc>

<MAIN>
<PRINTXCOMMENTS>

<IDXFNAME>
index.html
</IDXFNAME>

<definevar>
GRAY_SEARCH_FORM
<div class="search-form">
<form method="get" action="/mailman/namazu.cgi">
<span class="input-label">Search for:</span> 
<input type="text" name="query" size="40">
<input type="submit" name="submit" value="Go!">
<input type="hidden" name="idxname" value="$gray_listname$">
<a href="/mailman/namazu.cgi?idxname=$gray_listname$">Advanced</a>
</form>
</div>
</definevar>

<definevar chop>
PREV-PERIOD-LABEL
Prev Period
</definevar>

<definevar chop>
PREV-PERIOD-URL
$gray_base_url$?l=$gray_listname$&p=$gray_period$&d=prev
</definevar>

<definevar chop>
NEXT-PERIOD-LABEL
Next Period
</definevar>

<definevar chop>
NEXT-PERIOD-URL
$gray_base_url$?l=$gray_listname$&p=$gray_period$&d=next
</definevar>

<definevar chop>
TOP-URL
$gray_base_url$/$gray_audience$/$gray_listname$
</definevar>

<TITLE>
<a href="$TOP-URL$">$gray_listname$</a> Archive: $gray_period_pretty$
</TITLE>

<TTITLE>
<a href="$TOP-URL$">$gray_listname$</a> Archive: $gray_period_pretty$
</TTITLE>

<SPAMMODE>
<EXCS>
Organization
List-
X-
Mail-Followup-To
Reply-To
</EXCS>

<definevar>
BODY-HEAD
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rev="made" href="mailto:gray@gnu.org.ua" />
 <link rel="stylesheet" href="/private/list/mailist.css" type="text/css" />
</definevar>

<NODOC>

<definevar>
GRAY-FOOTER
<div id="footer">
<p>
<address>Mail converted by 
<a href="$DOCURL$">MHonArc</a></address>.
<span class="hint">Report problems and send sugestions to <a href="mailto:$ENV(SAVANE_MAILMAN_ADMIN_EMAIL)$">List Administrator</a>.</span>
</p>
</div>
</definevar>

<!-- ***********************
      Main index page layout
     *********************** -->

<IdxPgBegin>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>$gray_listname$: $gray_period_pretty$ [by date]</title>
$BODY-HEAD$
</head>
<body>
<h1>$IDXTITLE$</h1>
</IdxPgBegin>

<IdxPgEnd>
$GRAY-FOOTER$
</body>
</html>
</IdxPgEnd>

<ListBegin>
<ul class="idx-nav">
 <li><a href="$TOP-URL$">Top</a></li>
 <li class="active"><a href="$IDXFNAME$">$IDXLABEL$</a></li>
 <li><a href="$TIDXFNAME$">$TIDXLABEL$</a></li>
 <li class="period-link"><a href="$PREV-PERIOD-URL$&t=date">$PREV-PERIOD-LABEL$</a>
 <a href="$NEXT-PERIOD-URL$&t=date">$NEXT-PERIOD-LABEL$</a></li>
</ul>

$GRAY_SEARCH_FORM$

<ul class="date-list">
</ListBegin>

<ListEnd>
</ul>
</ListEnd>

<LiTemplate>
<li>$SUBJECT$, <i>$FROMNAME$</i>, <tt>$MSGLOCALDATE(CUR;%H:%M)$</tt></li>
</LiTemplate>

<DayBegin>
<li><strong>$MSGLOCALDATE(CUR;%B %d, %Y)$</strong>
<ul>
</DayBegin>

<DayEnd>
</ul>
</DayEnd>

<!-- **************************
      Thread index page layout
     ************************** -->

<TIdxPgBegin>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>$gray_listname$: $gray_period_pretty$ [by thread]</title>
$BODY-HEAD$
</head>
<body>
<h1>$TIDXTITLE$</h1>
</TIdxPgBegin>

<TIdxPgEnd>
$GRAY-FOOTER$
</body>
</html>
</TIdxPgEnd>

<THead>
<ul class="idx-nav">
 <li><a href="$TOP-URL$">Top</a></li>
 <li><a href="$IDXFNAME$">$IDXLABEL$</a></li>
 <li class="active"><a href="$TIDXFNAME$">$TIDXLABEL$</a></li>
 <li class="period-link"><a href="$PREV-PERIOD-URL$&t=thread">$PREV-PERIOD-LABEL$</a>
 <a href="$NEXT-PERIOD-URL$&t=thread">$NEXT-PERIOD-LABEL$</a></li>
</ul>

$GRAY_SEARCH_FORM$

<ul class="thr-list">
</THead>

<TFoot>
</ul>
</TFoot>

<TTopBegin>
<li><div class="thr-group">
<b>$SUBJECT$</b>, <i>$FROMNAME$</i>, <tt>$MSGLOCALDATE(CUR;%Y/%m/%d)$</tt>
</TTopBegin>

<TTopEnd>
</div></li>
</TTopEnd>

<TLiTxt>
<li><b>$SUBJECT$</b>, <i>$FROMNAME$</i>, <tt>$MSGLOCALDATE(CUR;%Y/%m/%d)$</tt>
</TLiTxt>

<TSingleTxt>
<li><div class="thr-single">
<b>$SUBJECT$</b>, <i>$FROMNAME$</i>, <tt>$MSGLOCALDATE(CUR;%Y/%m/%d)$</tt>
</div></li>
</TSingleTxt>

<!-- **************************
              Buttons
     ************************** -->
<NextButton chop>
<a href="$MSG(NEXT)$">Date Next</a>
</NextButton>

<NextButtonIA chop>
<span class="inactive">Date Next</span>
</NextButtonIA>

<PrevButton chop>
<a href="$MSG(PREV)$">Date Prev</a>
</PrevButton>

<PrevButtonIA chop>
<span class="inactive">Date Prev</span>
</PrevButtonIA>

<TEndButton chop>
<a href="$MSG(TEND)$">Last in Thread</a>
</TEndButton>

<TEndButtonIA>
<span class="inactive">Last in Thread</span>
</TEndButtonIA>

<TNextButton chop>
<a href="$MSG(TNEXT)$">Thread Next</a>
</TNextButton>

<TNextButtonIA>
<span class="inactive">Thread Next</span>
</TNextButtonIA>

<TNextInButton chop>
<a href="$MSG(TNEXTIN)$">Next in Thread</a>
</TNextInButton>

<TNextInButtonIA>
<span class="inactive">Next in Thread</span>
</TNextInButtonIA>

<TNextTopButton chop>
<a href="$MSG(TNEXTTOP)$">Next Thread</a>
</TNextTopButton>

<TNextTopButtonIA>
<span class="inactive">Next Thread</span>
</TNextTopButtonIA>

<TPrevButton chop>
<a href="$MSG(TPREV)$">Thread Prev</a>
</TPrevButton>

<TPrevButtonIA>
<span class="inactive">Thread Prev</span>
</TPrevButtonIA>

<TPrevInButton chop>
<a href="$MSG(TPREVIN)$">Prev in Thread</a>
</TPrevInButton>

<TPrevInButtonIA>
<span class="inactive">Prev in Thread</span>
</TPrevInButtonIA>

<TPrevTopButton chop>
<a href="$MSG(TPREVTOP)$">Prev Thread</a>
</TPrevTopButton>

<TPrevTopButtonIA>
<span class="inactive">Prev Thread</span>
</TPrevTopButtonIA>

<TTopButton chop>
<a href="$MSG(TTOP)$">First in Thread</a>
</TTopButton>

<TTopButtonIA>
<span class="inactive">First in Thread</span>
</TTopButtonIA>

<!-- Buttons End -->

<!-- **************************
         Message page layout
     ************************** -->
<MsgPgBegin>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>$SUBJECTNA$</title>
<link rev="made" href="mailto:$FROMADDR$">
$BODY-HEAD$
</head>
<body>
<h1>$gray_listname$</h1>
<hr>
</MsgPgBegin>

<MsgPgEnd>
$GRAY-FOOTER$
</body>
</html>
</MsgPgEnd>

<TopLinks>
<ul class="idx-nav">
 <li>$BUTTON(PREV)$</li>
 <li>$BUTTON(NEXT)$</li>
 <li>$BUTTON(TPREV)$</li>
 <li>$BUTTON(TNEXT)$</li>
 <li><a href="$IDXFNAME$#$MSGNUM$">Date Index</a></li>
 <li><a href="$TIDXFNAME$#$MSGNUM$">Thread Index</a></li>
</ul> 

$GRAY_SEARCH_FORM$
</TopLinks>

<SubjectHeader>
<h2>$SUBJECTNA$</h2>
</SubjectHeader>

<FieldsBeg>
<ul class="msg-header">
</FieldsBeg>

<FieldsEnd>
</ul>
</FieldsEnd>
