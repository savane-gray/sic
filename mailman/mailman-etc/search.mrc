<!-- x-search-form -->
<form method="get" action="/mailman/namazu.cgi">
<p><strong>Search String:</strong> 
<input type="text" name="query" size="40">
<input type="submit" name="submit" value="Search!">
<input type="hidden" name="idxname" value="$ENV(listname)$">
<a href="/mailman/namazu.cgi?idxname=$ENV(listname)$">[How to search]</a><br>

<strong>Display:</strong>
<select name="max">
<option value="10">10</option>
<option selected value="20">20</option>
<option value="30">30</option>
<option value="50">50</option>
<option value="100">100</option>
</select>
<strong>Description:</strong>
<select name="result">

<option selected value="normal">normal</option>
<option value="short">short</option>
</select>
<strong>Sort:</strong>
<select name="sort">
<option selected value="score">by score</option>
<option value="date:late">in reverse chronological order</option>
<option value="date:early">in chronological order</option>
<option value="field:subject:ascending">by title in ascending order</option>
<option value="field:subject:descending">by title in descending order</option>

<option value="field:from:ascending">by author in ascending order</option>
<option value="field:from:descending">by author in descending order</option>
<option value="field:size:ascending">by size in descending order</option>
<option value="field:size:descending">by size in descending order</option>
<option value="field:uri:ascending">by URI in descending order</option>
<option value="field:uri:descending">by URI in descending order</option>
</select>
</p>
</form>
<!-- /x-search-form -->
