# #######################################################################
# Stage 1:
# Build the necessary software in a separate image and install it under
# /install directory
# #######################################################################

ARG SIC_VERSION=latest
FROM graygnuorg/sic/core:$SIC_VERSION AS builder

ARG MAILMAN_VERSION=2.1.23
ARG MAILMAN_PREFIX=/opt/mailman
ARG MAILMAN_VAR_PREFIX=${MAILMAN_PREFIX}/var
ARG MAILMAN_GID=204
ARG MAILMAN_UID=204
ARG MAIL_GID=mail
ARG CGI_GID=www-data

RUN groupadd -g ${MAILMAN_GID} mailman && \
    useradd -u ${MAILMAN_UID} -d ${MAILMAN_PREFIX} -s /bin/false -g mailman mailman

# Create installation prefix and copy the user database to it.
RUN mkdir -p /install/etc
RUN cp /etc/passwd /etc/shadow /etc/group /install/etc

ARG BUILD_DEPS="build-essential git rsync autopoint automake autoconf libtool bison flex texinfo python-dev gettext patch"

RUN apt-get update -qq && \
    apt-get install -qq \
    python \
      python-mysqldb \
      python-dnspython \
      libfile-mmagic-perl \
      ${BUILD_DEPS}

RUN install -d ${MAILMAN_PREFIX} -g mailman -o root -m 2775 && \
    install -d ${MAILMAN_VAR_PREFIX} -g mailman -o root -m 2775
WORKDIR /usr/src

# Prepare the installation prefix for mailman and ensure correct
# ownership and mode
RUN mkdir -p /install/${MAILMAN_PREFIX} && \
    chgrp mailman /install/${MAILMAN_PREFIX} && \
    chmod a+rx,g+ws /install/${MAILMAN_PREFIX}

# Build and install mailman
RUN wget -nv http://ftp.gnu.org/gnu/mailman/mailman-${MAILMAN_VERSION}.tgz && \
    tar xf mailman-${MAILMAN_VERSION}.tgz && \
    cd mailman-${MAILMAN_VERSION} && \
    find messages -name '*.po' | while read name; do msgconv -t utf-8 -o $name.tmp $name && mv $name.tmp $name; done && \
    sed -i -r -e "s/^(add_language\('[^']+',[[:space:]]*_\('[^']+'\),[[:space:]]+)'[^']+'(,.*)/\1'utf-8'\2/" Mailman/Defaults.py.in && \
    ./configure \
         --prefix=${MAILMAN_PREFIX} \
         --exec-prefix=${MAILMAN_PREFIX} \
	 --with-var-prefix=${MAILMAN_VAR_PREFIX} \
	 --localstatedir=/var \
	 --with-mail-gid=${MAIL_GID} \
	 --with-cgi-gid=${CGI_GID} \
	 --without-permcheck && \
    make && \
    make install DESTDIR=/install

# Install additional scripts
RUN cd /install/${MAILMAN_PREFIX}/bin && \
    for i in erase list_requests discard_subs.py; \
    do \
      wget https://www.msapiro.net/scripts/$i; \
      chmod +x $i; \
    done

RUN chmod g+rws /install/opt/mailman/var/locks    

# Install Puszcza additional mailman software
RUN git clone git://git.gnu.org.ua/savane-gray/mailman.git && \
    cd mailman && \
    make config PREFIX=${MAILMAN_PREFIX} MAILMANSRC=/usr/src/mailman-${MAILMAN_VERSION} && \
    make install DESTDIR=/install

# Build and install namazu
COPY mknmz.diff File-MMagic.diff NMZ.head.diff MHonArc-2.6.19-defhash.diff /usr/src/
RUN wget -nv http://www.namazu.org/stable/namazu-2.0.21.tar.gz && \
    tar xf namazu-2.0.21.tar.gz && \
    cd namazu-2.0.21 && \
    patch -p1 < ../mknmz.diff && \
    patch -p1 < ../File-MMagic.diff && \
    patch -p1 < ../NMZ.head.diff && \
    cd File-MMagic && \
    perl Makefile.PL PREFIX=/usr INSTALLDIRS=vendor && \
    make && \
    make install DESTDIR=/install && \
    cd .. && \				 
    ./configure --prefix=${MAILMAN_PREFIX} --libexecdir=${MAILMAN_PREFIX}/cgi-bin && \
    make && \
    make install DESTDIR=/install

# Build and install MHonArc
RUN wget -nv https://www.mhonarc.org/release/MHonArc/tar/MHonArc-2.6.19.tar.gz && \
    tar xf MHonArc-2.6.19.tar.gz && \
    cd MHonArc-2.6.19 && \
    patch -p1 < ../MHonArc-2.6.19-defhash.diff && \
    perl Makefile.PL PREFIX=/usr INSTALLDIRS=vendor && \
    make install 'INSTALLPRG=install.me -root=/install'
    
# #######################################################################
# Stage 2:
# Create the actual mailman image.  Copy mailman software from builder.
# #######################################################################

ARG SIC_VERSION=latest
FROM graygnuorg/sic/core:$SIC_VERSION

COPY --from=builder /install /
RUN cp -r /usr/src/packages/nullmailer/* /

ARG PHP="php7.3\
 php7.3-mysql\
 php7.3-curl\
 php7.3-gd\
 php7.3-imagick\
 php7.3-intl\
 php7.3-mbstring\
 php7.3-xmlrpc\
 php7.3-xsl\
 php7.3-xml"

RUN apt-get update -qq && \
    apt-get install -qq \
       ca-certificates \
       nullmailer \
       lsof \
       apache2 \
       ${PHP} \
       python \
       mariadb-client \
       python-mysqldb \
       python-dnspython \
       perl \
       libfile-mmagic-perl \
       /usr/src/packages/micron*.deb \
       /usr/src/packages/rpipe*.deb \
       /usr/src/packages/syslogrelay*.deb

ENV SIC_SERVICE=mailman

COPY mm_cfg.py /opt/mailman/Mailman/mm_cfg.py.xenv
ADD mailman-etc/ /opt/mailman/etc
ADD tree/ /

VOLUME "/var/spool/nullmailer"
EXPOSE 1
EXPOSE 80

RUN apt-get purge -y --auto-remove \
    -o APT::AutoRemove::RecommendsImportant=false &&\
    rm -rf /var/lib/apt/lists/* /usr/src/packages

