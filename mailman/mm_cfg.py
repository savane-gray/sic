from Defaults import *
from os import getenv

DEFAULT_EMAIL_HOST = getenv('SAVANE_MAILMAN_DOMAIN')
DEFAULT_URL_HOST = getenv('SAVANE_MAILMAN_HOST')
DEFAULT_URL_PATTERN = 'https://%s/mailman/'

DELIVERY_MODULE = 'SMTPDirect'
SMTPHOST = getenv('SAVANE_MAIL_GATEWAY')

PUBLIC_ARCHIVE_URL  =  getenv('SAVANE_PUBLIC_ARCHIVE_URL') or 'https://%(hostname)s/mailman/listarchive/%(listname)s'
PRIVATE_ARCHIVE_URL  = getenv('SAVANE_PRIVATE_ARCHIVE_URL') or 'https://%(hostname)s/mailman/listarchive/%(listname)s'

PUBLIC_MBOX = No

# do not use builtin mailman html archiving, archive to mbox only
ARCHIVE_TO_MBOX = 1
DEFAULT_SEND_REMINDERS = 0

SUBSCRIBE_FORM_SECRET = "$(openssl rand -base64 18)"
